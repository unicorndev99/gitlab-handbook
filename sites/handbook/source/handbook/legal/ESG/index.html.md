---
layout: handbook-page-toc
title: "ESG"
description: "Information and processes related to ESG"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview
The Environmental, Social, and Governance (ESG) Team creates and maintains GitLab’s Corporate Sustainability strategy and programs. This includes ESG disclosures and public ESG reporting, identifying and prioritizing key issues to advance GitLab’s social and environmental goals, and creating partnerships with non-profit organizations that support GitLab’s values and mission.

## ESG Strategy
The [ESG Team](/job-families/legal-and-corporate-affairs/environmental-social-governance/) is in the process of conducting a formal materiality assessment to speak with GitLab’s internal and external stakeholders to identify and prioritize its key environmental, social and governance (ESG) issues. Stakeholders will provide insights and viewpoints on key issues for the company from both a financial materiality and impact materiality perspective. 

The results of the assessment will drive GitLab’s Corporate Sustainability strategy, key issues, and program development. 

Check back in early 2023 for updates. 
[Click here](/handbook/environmental-sustainability/) to view GitLab’s historical environmental sustainability efforts. 

## FAQ

**Q: Who can I contact for ESG-related questions?** 

A:  Senior Director, ESG/DRI: Stacy Cline - @slcline on GitLab.
Email: ESG@GitLab.com. 

**Q: Does GitLab calculate its carbon emissions?** 

A: GitLab will conduct a formal greenhouse gas inventory in 2023. [Click here](/handbook/environmental-sustainability/) for historical in-house estimates. 

**Q: Has GitLab set a Science Based Target (SBT)?**

A: Not yet. GitLab will conduct a formal greenhouse gas inventory in 2023. This inventory will measure our current emissions and help us identify opportunities for reductions. 


**Q: Does GitLab track employment by gender and ethnicity?**

A: Yes, [view the most recent identity data](/company/culture/inclusion/identity-data/). 

**Q: Does GitLab set goals to increase diversity?**

A: Yes, GitLab publishes [people success KPIs](/handbook/people-group/people-success-performance-indicators/#executive-summary). 

**Q: How does GitLab define underrepresented groups?**

A: An underrepresented group describes a subset of a population that holds a smaller percentage within a significant subgroup than the subset holds in the general population. View the full definition [here](/company/culture/inclusion/#examples-of-select-underrepresented-groups). 

**Q: Is GitLab certified as a diverse supplier?**

A: GitLab is a publicly traded company (NASDAQ: GTLB) and is not defined as a diverse supplier and is unable to be certified as such, accordingly. Nevertheless, diversity, inclusion, and belonging (DIB) is a [core value](https://about.gitlab.com/handbook/values/) at GitLab. On a daily basis, we strive to keep our operations, employment practices, and supplier selection in line with [this value](https://about.gitlab.com/company/culture/inclusion/). GitLab's [DIB team](https://about.gitlab.com/job-families/people-group/diversity-inclusion-partner/) builds an environment where all team members feel a [sense of belonging](https://about.gitlab.com/job-families/people-group/diversity-inclusion-partner/), which results in a truly inclusive and welcoming work environment. Moreover, GitLab's Procurement team selects potential suppliers with [responsible sourcing and diversity](https://about.gitlab.com/handbook/finance/procurement/) in mind.

**Q: Does GitLab sponsor/fund nonprofit organizations?**

A: GitLab does not currently have a formal corporate giving program. Pending the results of the 2022 materiality assessment, the first iteration of a corporate giving program is planned for 2023. 

**Q: Does GitLab donate its product to nonprofits?**

A: GitLab does not currently have a formal in-kind donation program. Pending the results of the 2022 materiality assessment, the first iteration of a non-profit in-kind donation program is planned for 2023. Learn about [other ways](/handbook/marketing/community-relations/community-programs/education-program/) that GitLab contributes its product to the community. 
